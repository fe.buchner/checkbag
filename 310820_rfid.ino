#include <SPI.h> //?
#include <MFRC522.h> //RfID-Erkennung

//#include <Wire.h>
#include <LiquidCrystal.h> 
LiquidCrystal lcd(12, 11, 8, 9, 10, 40);

//Including the two libraries
//#include <UniversalTelegramBot.h>

//#include <ESP8266WiFi.h>
//#include <WiFiClientSecure.h>

//#define TELEGRAM_BUTTON_PIN D5

// ------- Telegram config --------
//#define BOT_TOKEN "1339356834:AAF_Fl_hKzcHx97GmWnJUqi5--f0W_UhIDg"  // your Bot Token (Get from Botfather)
//#define CHAT_ID "1117010338" // Chat ID of where you want the message to go (You can use MyIdBot to get the chat ID)

// SSL client needed for both libraries
//WiFiClientSecure client;
//UniversalTelegramBot bot(BOT_TOKEN, client);

//long checkTelegramDueTime; 
//int checkTelegramDelay = 1000; 

//String defaultChatId = "1117010338"; 

//------- WiFi Settings -------
//char ssid[] = "o2_WLAN07";       // your network SSID (name)
//char password[] = "serssers";  // your network key

#define RST_PIN 5 //PIN Arduino
#define SS_PIN 53 //PIN Arduino

#define DEST_SIZE 40

MFRC522 mfrc522(SS_PIN, RST_PIN); //Leseobjekt 

byte key_uid[] = {0x69, 0xF9, 0x97, 0xB8}; //Standarduid
byte mask_uid[] = {0xA9, 0x3F, 0xD6, 0xC2}; 
byte wallet_uid[] = {0xD4, 0xD1, 0x04, 0x2A};
byte handy_uid[] = {0xE4, 0xAA, 0x4A, 0x2A}; 

int blue_led = 6; //blue led via pin 6, 0 & 1 do not work
int green_led = 2;
int red_led = 3;
int yellow_led = 4; 

int blue_check = false; //Hilfsvariable
int green_check = false;
int red_check = false;
int yellow_check = false; 

const int sound = 7; 

char missing = "MISSING"; 
char* missing_objects[] = {"KEY ", "MASK ", "WALLET ", "PHONE "};


char dest[DEST_SIZE];

void setup() {
  Serial.begin(9600); 
  SPI.begin();
  mfrc522.PCD_Init(); 
  pinMode(blue_led, OUTPUT);
  pinMode(green_led, OUTPUT);
  pinMode(red_led, OUTPUT);
  pinMode(yellow_led, OUTPUT);
  pinMode(sound, OUTPUT); 

  // LCD Display
  lcd.begin(16, 2);
  lcd.print("MISSING:");
  lcd.setCursor(0,1);
  lcd.print("KEY, MASK, WALLET, PHONE");
 

// --- WiFi setup--- //
 /** Serial.print("Connecting Wifi: ");
  Serial.print(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status()!= WL_CONNECTED) {
    Serial.print(".");
    delay(500);
    }
  Serial.println(" ");
  Serial.println("WiFi connected");
  Serial.println("IP Address: ");
  IPAddress ip = WiFi.localIP();
  Serial.println(ip);*/
} 

void loop() {

  if(mfrc522.PICC_IsNewCardPresent() && mfrc522.PICC_ReadCardSerial() ) {
    Serial.print("Gelesene UID: "); 
    for(byte i=0; i < mfrc522.uid.size; i++){
      Serial.print(mfrc522.uid.uidByte[i] < 0x10 ? " 0": " "); 
      Serial.print(mfrc522.uid.uidByte[i], HEX); 
    }
    Serial.println(); 

    //LEDs
    blue_check = true; 
    green_check = true; 
    red_check = true;
    yellow_check = true; 
    
    for(int j=0; j<4; j++){
      if(mfrc522.uid.uidByte[j] != key_uid[j]){
        blue_check = false; 
      }
      
      if(mfrc522.uid.uidByte[j] != mask_uid[j]){
        green_check = false; 
      }
      
      if(mfrc522.uid.uidByte[j] != wallet_uid[j]){
        red_check = false; 
      }

      if(mfrc522.uid.uidByte[j] != handy_uid[j]){
        yellow_check = false; 
      }
    }

    if(blue_check or green_check or red_check or yellow_check){
      tone(sound, 1000); // Tonhöhe 1000
      delay(1000); //Dauer
      noTone(sound); //aus 
    }
    
    if(blue_check){
      if(digitalRead(blue_led) == HIGH){
        digitalWrite(blue_led, LOW); 
        display_missing_function(); 
      }
      else{
        digitalWrite(blue_led, HIGH);
        display_check_function("KEY"); 
        display_missing_function(); 
      }
    }
    else if(green_check){
      if(digitalRead(green_led) == HIGH){
        digitalWrite(green_led, LOW); 
        display_missing_function(); 
      }
      else{
        digitalWrite(green_led, HIGH);
        display_check_function("MASK");
        display_missing_function();  
      } 
    }
    else if(red_check){
      if(digitalRead(red_led) == HIGH){
        digitalWrite(red_led, LOW); 
        display_missing_function(); 
      }
      else{
        digitalWrite(red_led, HIGH);
        display_check_function("WALLET");
        display_missing_function(); 
      } 
    }
    else if(yellow_check){
      if(digitalRead(yellow_led) == HIGH){
        digitalWrite(yellow_led, LOW);
        display_missing_function();  
      }
      else{
        digitalWrite(yellow_led, HIGH);
        display_check_function("PHONE");
        display_missing_function(); 
      }
    }

    mfrc522.PICC_HaltA(); //freischalten für nächste Karte
    delay(1000); // loop nach delay
  }
}

/**
 * Dise Funktion zeigt das Object das in die Tasche getan wurde auf dem Display an. 
 */
void display_check_function(char object[8]){
  lcd.clear(); 
  lcd.print(object); 
  lcd.setCursor(0,1); 
  lcd.print("CHECK");
  delay(3000); 
  lcd.clear();
}

/**
 * Diese Funktion zeigt alle Objekte die nicht in der Tasche sind auf dem Bildschirm an. 
 */
void display_missing_function(){
  lcd.setCursor(0,0);
  int scroll_number = 8; 
  char text1[35] = "MISSING: ";
  if(digitalRead(blue_led) == LOW){
    char text2[5] = "KEY ";
    strcat(text1, text2);
    scroll_number = scroll_number + 4; 
   }
  if(digitalRead(green_led) == LOW){
    char text3[5] = "MASK ";
    strcat(text1, text3);
    scroll_number = scroll_number + 5;
   }
  if(digitalRead(red_led) == LOW){
    char text4[15] = "WALLET ";
    strcat(text1, text4);
    scroll_number = scroll_number + 7;
   }
  if(digitalRead(yellow_led) == LOW){
    char text5[10] = "PHONE ";
    strcat(text1, text5);
    scroll_number = scroll_number + 6;
   }
  lcd.print(text1);
  //Scrollen nach links
  if(scroll_number > 15){
    for (int positionCounter = 0; positionCounter < (scroll_number - 15); positionCounter++) {
      delay(1000);
      lcd.scrollDisplayLeft();
    } 
  } 
  else{
    delay(2000);
  }
  lcd.clear(); 
}
